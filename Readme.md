# Side notes for lab testing

## Sequence to do the labs:

1. Creating_Watson_Virtual_Assistant.md
link to the file <a href="https://gitlab.com/samaahs/microbachelors_capstone/-/blob/main/Creating_Watson_Virtual_Assistant.md"> Here </a>

2. Adding_Skill_to_Your_Watson_Virtual_Assistant.md
Link to the file <a href="https://gitlab.com/samaahs/microbachelors_capstone/-/blob/main/Adding_Skill_to_Your_Watson_Virtual_Assistant.md"> Here </a>

## Please download the JSON file for uploading as the link within the lab wont download it.
<a href="https://gitlab.com/samaahs/microbachelors_capstone/-/blob/main/MyWebBrowserBot-dialog.json">
CLICK HERE TO GET REDIRECTED </a>

