<img src="images/IDSNlogo.png" width="200" height="200"/>
<br> <br> <br> 

# Hands-on lab: Creating Watson Virtual Assitant

**Estimated Effort:** 20 minutes

## Lab overview:

IBM Watson Virtual Assistant lets you build, train, and deploy conversational interactions into any application, device or channel. To create the Watson Virtual Assistant service, you will need to have an IBM Cloud account. This labs takes you the process of creating the Virtual Assistant.

## Objectives:

After completing this lab, you will be able to:

*   Provision an instance of Watson Virtual Assistant

# Exercise 1: Create an IBM Cloud Account

### Scenario

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. If you have not created one already, click on this [link](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/CreateIBMCloudAccountForCognitive.md.html) and follow the instructions to create an IBM Cloud account.

### Prerequisites

If you already have an IBM Cloud account, you can skip this part and proceed with *Task 1: Login to you IBM Cloud account.*


## Task 1 - Create a Watson Assistant service


In this task, we are going to create .

1.  **Visit** [**https://cloud.ibm.com**](https://cloud.ibm.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMCD0351ENedX25277686-2021-01-01) **and log in** with your IBM Cloud email and password.

2.  You'll find yourself in your IBM Cloud dashboard. Click on the **Create resource**  **button**  on your dashboard, as shown in the picture below.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CD0351EN-edX/labs/module_1_overview_prework/images/create_resource.png">


3. Click on the  **Watson Assistant** tile on the resources page or enter **Watson Assistant** on the search box and select.

<img src="images/CWA_Task_1.3.png">

4.  You should see a Watson Assistant creation page similar to the image below.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CD0351EN-edX/labs/module_1_overview_prework/images/choose_region_for_WA.PNG">

5.  **Click on the region drop down to select a data center closer to you**. For example, you might **select Frankfurt** if you live in Europe. This will reduce latency and improve performance as you use Watson Assistant. Click on the **Create** **button** to create your instance.

6.  You'll be redirected to the launch page for the service you just created.  Click on the **Launch Watson Assistant** button to access the web application that will allow you to create chatbots.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CD0351EN-edX/labs/module_1_overview_prework/images/launch_watson_assitant.png">

> **NOTE:** If you are on classic experience, Please switch it to new experience.

7. Name your assistant **MyWebServeBot** or anything appropriate and click on create.

<img src="images/CWA_Task_1.7.png">

8. On successfully creating your assistant, it will look something like this:

<img src="images/CWA_Task_1.8.png">

Congratulations on creating your instance of Watson Assistant!

## Author(s)

[Lavanya](https://www.linkedin.com/in/lavanya-sunderarajan-199a445/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMCD0351ENedX25277686-2021-01-01)

## Changelog

| Date       | Version | Changed by | Change Description                           |
| ---------- | ------- | ---------- | -------------------------------------------- |
| 2022-02-23 | 2.0     | Samaah    | Updated lab Instructions and Images |
| 2021-03-23 | 1.0     | Lavanya   | Created lab for IBM Microbachelors  Capstone |
|            |         |            |                                              |

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>


