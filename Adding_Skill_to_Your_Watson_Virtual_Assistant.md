<img src="images/IDSNlogo.png" width="200" height="200"/>
<br> <br> <br> 

# Hands-on lab on Adding skill to your Watson Virtual Assitant

IBM Watson Virtual Assistant lets you build, train, and deploy conversational interactions into any application, device or channel. This lab will guide you through the process of adding skill to your Watson Assistant.

## Duration (25 mins)

## Objective

After completing this lab you will be able to:

1. Add new skills to the instance of your Watson Virtual Assistant

## Task 1 - Opening Assistant

In this task, we are going to create .

1. Login to IBM Cloud with your credentials. If you do not have an IBM Cloud account, please complete the prework and come back to this page.

2. Go to your Resource List, and from the list of services shown, choose  **Watson Assistant**.

<img src="images/Skill_Task_1.2.png" style="margin-left:30"/>

3. Click on **Launch Assistant**, to open the Watson Asssistant resource. 

<img src="images/Skill_Task_1.3.png" style="margin-left:50"/><br>

 
## Task 2 - Adding New Skills to you assistant

> **NOTE:** If you are on classic experience, Please switch it to new experience. And in case of multiple assistants created, please select the one you saved as *MyWebServerBot*

1. Now while you are on your *MyWebServerBot* assistant, Click on **Action** from the panel on the left (marked in RED) and then click on the **Global Settings** on the top right (marked in GREEN). 

<img src="images/Skill_Task_2.1.png" style="margin-left:50"/>

Using the global settings, you can train your assistant if you have a predefined JSON with all the skills. And there are other options to improve your assistant too.

2. Click <a href="MyWebBrowserBot-dialogTest.json">here </a> to download the JSON file

3. Select Upload/Download and choose the file that you downloaded in the previous step and click on Upload.
<img src="images/Skill_Task_2.3.png" style="margin-left:50"/>

4. Now click on *upload and replace*. This will remove the default/previous skills of the bot and replace it with the new ones that are being uploaded in the form of JSON.

<img src="images/Skill_Task_2.4.png" style="margin-left:50"/>

5. Once the skill is uploaded, and success pop up is displayed. Close the Global settings. You will see the new actions added up.

<img src="images/Skill_Task_2.5.png" style="margin-left:50"/>

## Task 3 - Add new action skill manually to the assistant.

Let's try enhancing the user experience my adding up more questions to the assistant.

1. Under Actions -> Created by you, Click on the **#Help** Action. The help action looks like this

Click on Show more examples to add a new question.
<img src="images/Skill_Task_3.1.png" style="margin-left:50"/>

2. In the *Enter a new Phrase* we write a question that a user may ask according to which the assistant replies.
In the input box, write `**What all can you assist me with?**`

<img src="images/Skill_Task_3.2.png" style="margin-left:50"/>

3. Save the action by clicking save on top right. Wait for the system to train itself and then close the action. 


## Task 4 - Test your assistant

1.  Click on preview button from the bottom left to test your assistant. And type `**What all can you assist me with?**`

<img src="images/Skill_Task_4.1.png" style="margin-left:50"/>

Congratulations on completing this lab!

## Author(s)

[Lavanya](https://www.linkedin.com/in/lavanya-sunderarajan-199a445/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMCD0351ENedX25277686-2021-01-01)

## Changelog

| Date       | Version | Changed by | Change Description                           |
| ---------- | ------- | ---------- | -------------------------------------------- |
| 2022-02-23 | 2.0     | Samaah    | Updated lab Instructions and Images |
| 2021-03-31 | 1.0     | Lavanya   | Created lab for IBM Microbachelors  Capstone |
|            |         |            |                                              |

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. </h3>


